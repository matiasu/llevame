<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\LogisticoSearch $searchModel
 */

$this->title = 'Logisticos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistico-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Logistico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idLogistico',
            'idEmpLog',
            'idUsuario',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
