<?php

namespace app\controllers;

use Yii;
use app\models\Voucher;
use app\models\VoucherSearch;
use app\models\Trabaja;
use app\models\Servicio;
use app\models\Empresaoperadora;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * VoucherprestadorController implements the CRUD actions for Voucher model.
 */
class VoucherprestadorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['prestador'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex($idPrestador,$idEmpOp)
    {
        Yii::$app->session['idPrestador']=$idPrestador;
        Yii::$app->session['idEmpOp']=$idEmpOp;
        Yii::$app->session['rol']="prestador";
        $searchModel = new VoucherSearch;
        $dataProvider = $searchModel->search(['VoucherSearch'=>['idPrestador'=>$idPrestador]]); //filtra los vouchers levantados por el prestador

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'idEmpOp' => $idEmpOp,
        ]);
    }

    /**
     * Displays a single Voucher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idServicio,$idLogistico,$idEmpLog)
    {
        //$usuario = ;
        $emOp = Trabaja::findAll($idEmpLog);
        $model = new Voucher;
        $model->idLogistico = $idLogistico;
        $model->idServicio = $idServicio;
        $model->idEmpLog = $idEmpLog;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idVoucher]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'empOp' => $emOp,
                
            ]);
        }
    }

    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->idPrestador = Yii::$app->session['idPrestador'];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idVoucher]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Voucher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voucher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
