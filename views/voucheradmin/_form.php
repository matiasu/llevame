<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Trabaja;
use app\models\Estado;
use kartik\date\DatePicker;
use kartik\time\TimePicker;

/**
 * @var yii\web\View $this
 * @var app\models\Voucher $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="voucher-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <?= $form->field($model, 'origen')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'destino')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'fecha')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Fecha de viaje'],
        'pluginOptions' => [
            'autoclose'=>true,
            'language' => 'es',
            'format' => 'yyyy/mm/dd'
    ]
    ]);  ?>

    <?= $form->field($model, 'hora')->widget(TimePicker::className()) ?>

    

    <?= $form->field($model, 'idEstado')->textInput() ?>

    <?= "" //$form->field($model, 'idServicio')->textInput() ?>

    <?= $form->field($model, 'idEmpOp')->dropDownList(ArrayHelper::map(Trabaja::find()->all(),'idEmpOperadora','idEmpOperadora0.nombre')) ?>

    <?= "" //$form->field($model, 'idEmpLog')->textInput() ?>

    <?= "" //$form->field($model, 'idOperador')->textInput() ?>

    <?= "" // $form->field($model, 'idPrestador')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
