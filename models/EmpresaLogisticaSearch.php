<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpresaLogistica;

/**
 * EmpresaLogisticaSearch represents the model behind the search form about `app\models\EmpresaLogistica`.
 */
class EmpresaLogisticaSearch extends EmpresaLogistica
{
    public function rules()
    {
        return [
            [['idEmpLogistica'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EmpresaLogistica::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEmpLogistica' => $this->idEmpLogistica,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
