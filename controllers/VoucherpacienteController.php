<?php

namespace app\controllers;

use Yii;
use app\models\Voucher;
use app\models\VoucherSearch;
use app\models\Trabaja;
use app\models\Servicio;
use app\models\Prestador;
use app\models\ServicioSearch;
use app\models\Empresalogistica;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * VoucherController implements the CRUD actions for Voucher model.
 */
class VoucherpacienteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','index','view','voucher'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex($idEmpOp,$idPaciente)
    {
        Yii::$app->session['idPaciente']=$idPaciente;
        Yii::$app->session['idEmpOp']=$idEmpOp;
        $servicio = Servicio::findOne(['idEmpOp'=>$idEmpOp,'idPaciente'=>$idPaciente]);
        if (is_null($servicio)){
            throw new NotFoundHttpException('El paciente no tiene servicios asociados');
        }
        //print_r($servicio->idServicio);
        //$dataServicioProvider = $searchServicio->search(['ServicioSearch'=>['idPaciente'=>$idPaciente],['idEmpOp'=>$idEmpOp]]);
        $searchModel = new VoucherSearch;
        $dataProvider = $searchModel->search(['VoucherSearch'=>['idServicio'=>$servicio->idServicio, 'idEstado'=>1]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            
        ]);
    }
    public function actionVoucher($idPaciente){
        Yii::$app->session['idPaciente']=$idPaciente;
        $servicio = Servicio::findOne(['idPaciente'=>$idPaciente,'idEmpOp'=>Yii::$app->session['$idEmpOp']]);
        if (is_null($servicio)){
            throw new NotFoundHttpException('El paciente no tiene servicios asociados');
        }
        $searchModel = new VoucherSearch;
        $dataProvider = $searchModel->search(['VoucherSearch'=>['idServicio'=>$servicio->idServicio,'idEstado'=>1]]);

        return $this->render('voucher', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            
        ]);
    }
    /**
     * Displays a single Voucher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idServicio,$idLogistico,$idEmpLog)
    {
        //$usuario = ;
        $emOp = Trabaja::findAll($idEmpLog);
        $model = new Voucher;
        $model->idLogistico = $idLogistico;
        $model->idServicio = $idServicio;
        $model->idEmpLog = $idEmpLog;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idVoucher]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'empOp' => $emOp,
                
            ]);
        }
    }
    
     
    
    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$nombre = Yii::$app->user->identity->username;
        //$prestador = Prestador::findOne($nombre);
        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->session['$idEmpOp']==$model->idEmpOp){
                throw new ForbiddenHttpException('El paciente no tiene servicios asociados');
            }else{
                $model->idPrestador = Yii::$app->session['idPrestador'];
                $model->idEstado = 2;
                $model->save();
                return $this->redirect(['view', 'id' => $model->idVoucher]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Voucher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voucher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
