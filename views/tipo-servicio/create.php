<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\tipoServicio $model
 */

$this->title = 'Crear Tipo de Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-servicio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
