<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\EmpresaLogistica;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Logistico $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="logistico-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEmpLog')->dropDownList(ArrayHelper::map(EmpresaLogistica::find()->all(),'idEmpLogistica','nombre')) ?>

    <?= $form->field($model, 'idUsuario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
