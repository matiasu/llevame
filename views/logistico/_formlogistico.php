<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use app\models\Logistico;
use yii\helpers\ArrayHelper;
use app\models\EmpresaLogistica;


/**
 * @var yii\web\View $this
 * @var app\models\Logistico $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="logistico-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEmpresaLog')->dropDownList(ArrayHelper::map(EmpresaLogistica::find()->all(),'idEmpLogistica','nombre')) ?>

    <?= $form->field($model, 'idUsuario')->dropDownList(ArrayHelper::map(Usuario::find()->all(),'idUsuario','nombre')) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
