<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Html::img('../web/imagen/llevame_min.png',['width'=>'90px']),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $items=[
                ['label' => 'Inicio', 'url' => ['/site/index']],
                ['label' => 'Llevame', 'url' => ['/site/about']],
                ['label' => 'Contacto', 'url' => ['/site/contact']]
            ];
            
            if(!Yii::$app->user->isGuest ){
                if(Yii::$app->user->identity->esAdm){
                    $subitems = [];
                    $subitems[]=['label' => 'Empresa Logistica','url' => ['/empresa-logistica']];
                    $subitems[]=['label' => 'Empresa Operadora','url' => ['/empresa-operadora']];
                    $subitems[]=['label' => 'Usuario','url' => ['/usuario']];
                    $subitems[]=['label' => 'Logistico','url' => ['/logistico']];
                    $subitems[]=['label' => 'Operador','url' => ['/operador']];
                    $subitems[]=['label' => 'Prestador','url' => ['/prestador']];
                    $subitems[]=['label' => 'Paciente','url' => ['/paciente']];
                    $subitems[]=['label' => 'Trabaja','url' => ['/trabaja']];
                    $subitems[]=['label' => 'Tipo Servicio','url' => ['/tipo-servicio']];
                    $subitems[]=['label' => 'Brinda','url' => ['/brinda']];
                    $subitems[]=['label' => 'Estado de voucher','url' => ['/estado']];
                    $subitems[]=['label' => 'Voucher','url' => ['/voucheradmin']];
                    $subitems[]=['label' => 'Usuario','url' => ['/usuario']];
                    $subitems[]=['label' => 'Servicio','url' => ['/servicio']];
                    $items[]=['label' => 'Admin',
                                'items' => $subitems,
                               ];
                }
                //$items[]=['label' => 'Voucher','url' => ['/voucher']];
                //$items[]=['label' => 'Empresa Operadora','url' => ['/empresa-operadora']];
                //$items[]=['label' => 'Paciente','url' => ['/paciente']];
                //$items[]=['label' => 'Usuario','url' => ['/usuario']];
                //$items[]=['label' => 'Empresa Logistica','url' => ['/empresa-logistica']];
                //$items[]=['label' => 'Paciente','url' => ['/paciente']];
                
                $items[]=['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']];
            }else{
                $items[]=['label' => 'Login', 'url' => ['/site/login']] ;
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Llevame! <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
