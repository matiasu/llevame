<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trabaja;

/**
 * TrabajaSearch represents the model behind the search form about `app\models\Trabaja`.
 */
class TrabajaSearch extends Trabaja
{
    public function rules()
    {
        return [
            [['idEmpOperadora', 'idEmpLogistica'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Trabaja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEmpOperadora' => $this->idEmpOperadora,
            'idEmpLogistica' => $this->idEmpLogistica,
        ]);

        return $dataProvider;
    }
}
