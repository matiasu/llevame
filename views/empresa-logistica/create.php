<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EmpresaLogistica $model
 */

$this->title = 'Crear Empresa Logistica';
$this->params['breadcrumbs'][] = ['label' => 'Empresas Logisticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-logistica-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
