<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empresalogistica;
use app\models\Empresaoperadora;

/**
 * @var yii\web\View $this
 * @var app\models\Trabaja $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="trabaja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEmpOperadora')->dropDownList(ArrayHelper::map(Empresaoperadora::find()->all(),'idEmpOperadora','nombre')) ?>

    <?= $form->field($model, 'idEmpLogistica')->dropDownList(ArrayHelper::map(Empresalogistica::find()->all(),'idEmpLogistica','nombre')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
