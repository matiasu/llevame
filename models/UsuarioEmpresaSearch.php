<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsuarioEmpresa;

/**
 * UsuarioEmpresaSearch represents the model behind the search form about `app\models\UsuarioEmpresa`.
 */
class UsuarioEmpresaSearch extends UsuarioEmpresa
{
    public function rules()
    {
        return [
            [['idUsuario', 'idEmpLog', 'idEmpOp'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsuarioEmpresa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idUsuario' => $this->idUsuario,
            'idEmpLog' => $this->idEmpLog,
            'idEmpOp' => $this->idEmpOp,
        ]);

        return $dataProvider;
    }
}
