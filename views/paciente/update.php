<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Paciente $model
 */

$this->title = 'Actualizar Paciente: ' . $model->idPaciente;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPaciente, 'url' => ['view', 'id' => $model->idPaciente]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="paciente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
