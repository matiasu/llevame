<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EmpresaOperadora $model
 */

$this->title = 'Actualizar Empresa Operadora: ' . $model->idEmpOperadora;
$this->params['breadcrumbs'][] = ['label' => 'Empresas Operadoras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEmpOperadora, 'url' => ['view', 'id' => $model->idEmpOperadora]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="empresa-operadora-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
