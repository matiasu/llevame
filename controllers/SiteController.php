<?php

namespace app\controllers;

use Yii;
use app\models\EmpresaLogistica;
use app\models\EmpresaLogisticaSearch;
use app\models\Logistico;
use app\models\Prestador;
use app\models\Operador;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index'],
                'rules' => [
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $id = Yii::$app->user->id;
//        $searchUsuLogModel = new
        
        $logisticos = Logistico::find()->where('idUsuario=:id',[':id'=>$id])->all();
        $operadores = Operador::find()->where('idUsuario=:id',[':id'=>$id])->all();
        $prestadores = Prestador::find()->where('idUsuario=:id',[':id'=>$id])->all();
        return $this->render('index',
                [
                    'logisticos'=>$logisticos,
                    'operadores'=>$operadores,
                    'prestadores'=>$prestadores,
                ]
                );
                
                
                //'modelUsuLog' => $modelUsuLog,
                
               // );
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
