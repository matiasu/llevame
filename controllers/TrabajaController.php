<?php

namespace app\controllers;

use Yii;
use app\models\Trabaja;
use app\models\TrabajaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TrabajaController implements the CRUD actions for Trabaja model.
 */
class TrabajaController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [    
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trabaja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrabajaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Trabaja model.
     * @param integer $idEmpOperadora
     * @param integer $idEmpLogistica
     * @return mixed
     */
    public function actionView($idEmpOperadora, $idEmpLogistica)
    {
        return $this->render('view', [
            'model' => $this->findModel($idEmpOperadora, $idEmpLogistica),
        ]);
    }

    /**
     * Creates a new Trabaja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trabaja;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idEmpOperadora' => $model->idEmpOperadora, 'idEmpLogistica' => $model->idEmpLogistica]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Trabaja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idEmpOperadora
     * @param integer $idEmpLogistica
     * @return mixed
     */
    public function actionUpdate($idEmpOperadora, $idEmpLogistica)
    {
        $model = $this->findModel($idEmpOperadora, $idEmpLogistica);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idEmpOperadora' => $model->idEmpOperadora, 'idEmpLogistica' => $model->idEmpLogistica]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Trabaja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idEmpOperadora
     * @param integer $idEmpLogistica
     * @return mixed
     */
    public function actionDelete($idEmpOperadora, $idEmpLogistica)
    {
        $this->findModel($idEmpOperadora, $idEmpLogistica)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trabaja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idEmpOperadora
     * @param integer $idEmpLogistica
     * @return Trabaja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idEmpOperadora, $idEmpLogistica)
    {
        if (($model = Trabaja::findOne(['idEmpOperadora' => $idEmpOperadora, 'idEmpLogistica' => $idEmpLogistica])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
