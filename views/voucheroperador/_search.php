<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\VoucherSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="voucher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idVoucher') ?>

    <?= $form->field($model, 'origen') ?>

    <?= $form->field($model, 'destino') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'hora') ?>

    <?php // echo $form->field($model, 'idLogistico') ?>

    <?php // echo $form->field($model, 'idOperador') ?>

    <?php // echo $form->field($model, 'idPrestador') ?>

    <?php // echo $form->field($model, 'idEstado') ?>

    <?php // echo $form->field($model, 'idServicio') ?>

    <?php // echo $form->field($model, 'idEmpOp') ?>

    <?php // echo $form->field($model, 'idEmpLog') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
