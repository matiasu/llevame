<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Voucher $model
 */

$this->title = $model->idVoucher;
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index', 'idLogistico' => Yii::$app->session['idLogistico'], 'idEmpLog' => Yii::$app->session['idEmpLog']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idVoucher], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idVoucher], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idVoucher',
            'origen',
            'destino',
            'fecha',
            'hora',
            'idLogistico0.idUsuario0.nombre',
            'idOperador0.idUsuario0.nombre',
            'idPrestador0.idUsuario0.nombre',
            'idEstado0.estado',
            'idServicio0.detalles',
            'idEmpOp0.nombre',
            'idEmpLog0.nombre',
        ],
    ]) ?>

</div>
