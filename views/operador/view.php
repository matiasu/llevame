<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Operador $model
 */

$this->title = $model->idOperador;
$this->params['breadcrumbs'][] = ['label' => 'Operadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operador-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idOperador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idOperador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idOperador',
            'idEmpOp0.nombre',
            'idUsuario0.nombre',
            'idUsuario0.apellido',
        ],
    ]) ?>

</div>
