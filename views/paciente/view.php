<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\qr\QRCode;


/**
 * @var yii\web\View $this
 * @var app\models\Paciente $model
 */

$this->title = $model->idPaciente;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idPaciente], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idPaciente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPaciente',
            'nombre',
            'apellido',
            'dni',
            'mail',
        ],
    ]) ?>
    
    <?= QRCode::widget([
            'content' => 'http://http://meta.fi.uncoma.edu.ar/llevame/web/index.php?r=voucherpaciente/voucher&idPaciente='.$model->idPaciente, // qrcode data content
            'filename' => $model->nombre.'-'.$model->apellido.'-'.$model->dni.'.png', // image name (make sure it should be .png)
            'width' => '200', // qrcode image height 
            'height' => '200', // qrcode image width 
            'encoding' => 'UTF-8', // qrcode encoding method 
            'correction' => 'H', // error correction level (L,M,Q,H)
            'watermark' => 'No' // set Yes if you want watermark image in Qrcode else 'No'. for 'Yes', you need to set watermark image $stamp in QRCode.php
        ]);
    ?>
    
    

</div>
