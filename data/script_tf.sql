SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';



-- -----------------------------------------------------
-- Table `llevame`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`usuario` ;

CREATE TABLE IF NOT EXISTS `llevame`.`usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  `esAdm` TINYINT(1) NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`usuario` VALUES 
	(1,'admin','istrador','admin',True),
	(2,'logistico','logis','logistico',null),
	(3,'operador','oper','operador',null),
	(4,'prestador','presta','prestador',null);
-- -----------------------------------------------------
-- Table `llevame`.`empresaLogistica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`empresaLogistica` ;

CREATE TABLE IF NOT EXISTS `llevame`.`empresaLogistica` (
  `idEmpLogistica` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEmpLogistica`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`empresaLogistica` VALUES
	(1,'COAR'),
	(2,'SOMNIA');
-- -----------------------------------------------------
-- Table `llevame`.`empresaOperadora`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`empresaOperadora` ;

CREATE TABLE IF NOT EXISTS `llevame`.`empresaOperadora` (
  `idEmpOperadora` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEmpOperadora`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`empresaOperadora` VALUES
	(1,'SETRACO'),
	(2,'25 de Mayo');
-- -----------------------------------------------------
-- Table `llevame`.`logistico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`logistico` ;

CREATE TABLE IF NOT EXISTS `llevame`.`logistico` (
  `idLogistico` INT NOT NULL AUTO_INCREMENT,
  `idEmpLog` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idLogistico`),
  
    FOREIGN KEY (`idUsuario`)
    REFERENCES `llevame`.`usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpLog`)
    REFERENCES `llevame`.`empresaLogistica` (`idEmpLogistica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idUsuario_idx` ON `llevame`.`logistico` (`idUsuario` ASC);

CREATE INDEX `idEmpresaLog_idx` ON `llevame`.`logistico` (`idEmpLog` ASC);

INSERT INTO `llevame`.`logistico` VALUES
	(1,1,2),
	(2,2,2);
-- -----------------------------------------------------
-- Table `llevame`.`prestador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`prestador` ;

CREATE TABLE IF NOT EXISTS `llevame`.`prestador` (
  `idPrestador` INT NOT NULL AUTO_INCREMENT,
  `idEmpOp` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idPrestador`),
  
    FOREIGN KEY (`idEmpOp`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idUsuario`)
    REFERENCES `llevame`.`usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idEmpOp_idx` ON `llevame`.`prestador` (`idEmpOp` ASC);

CREATE INDEX `idUsuario_idx` ON `llevame`.`prestador` (`idUsuario` ASC);

INSERT INTO `llevame`.`prestador` VALUES
	(1,1,4),
	(2,2,4);
-- -----------------------------------------------------
-- Table `llevame`.`operador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`operador` ;

CREATE TABLE IF NOT EXISTS `llevame`.`operador` (
  `idOperador` INT NOT NULL AUTO_INCREMENT,
  `idEmpOp` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idOperador`),
  
    FOREIGN KEY (`idEmpOp`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idUsuario`)
    REFERENCES `llevame`.`usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idEmpOp_idx` ON `llevame`.`operador` (`idEmpOp` ASC);

CREATE INDEX `idUsuario_idx` ON `llevame`.`operador` (`idUsuario` ASC);

INSERT INTO `llevame`.`operador` VALUES
	(1,1,3),
	(2,2,3);
-- -----------------------------------------------------
-- Table `llevame`.`estado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`estado` ;

CREATE TABLE IF NOT EXISTS `llevame`.`estado` (
  `idEstado` INT NOT NULL AUTO_INCREMENT,
  `estado` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEstado`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`estado` VALUES
	(1,'Activo'),
	(2,'Utilizado');
-- -----------------------------------------------------
-- Table `llevame`.`paciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`paciente` ;

CREATE TABLE IF NOT EXISTS `llevame`.`paciente` (
  `idPaciente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `dni` INT NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPaciente`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`paciente` VALUES 
	(1,'Matias','Urrutia',111,'urrutia.mati@gmail.com');


-- -----------------------------------------------------
-- Table `llevame`.`tipoServicio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`tipoServicio` ;

CREATE TABLE IF NOT EXISTS `llevame`.`tipoServicio` (
  `idTipoServicio` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idTipoServicio`))
ENGINE = InnoDB;

INSERT INTO `llevame`.`tipoServicio` VALUES
	(1,'Taxi'),
	(2,'Remisse');
-- -----------------------------------------------------
-- Table `llevame`.`servicio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`servicio` ;

CREATE TABLE IF NOT EXISTS `llevame`.`servicio` (
  `idServicio` INT NOT NULL AUTO_INCREMENT,
  `detalles` VARCHAR(45) NOT NULL,
  `creditos` INT NULL,
  `idPaciente` INT NOT NULL,
  `idTipoServicio` INT NOT NULL,
  `idEmpLog` INT NOT NULL,
  `idEmpOp` INT NOT NULL,
  PRIMARY KEY (`idServicio`),
  
    FOREIGN KEY (`idPaciente`)
    REFERENCES `llevame`.`paciente` (`idPaciente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idTipoServicio`)
    REFERENCES `llevame`.`tipoServicio` (`idTipoServicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpLog`)
    REFERENCES `llevame`.`empresaLogistica` (`idEmpLogistica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpOp`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idPaciente_idx` ON `llevame`.`servicio` (`idPaciente` ASC);

CREATE INDEX `idTipoServicio_idx` ON `llevame`.`servicio` (`idTipoServicio` ASC);

CREATE INDEX `idEmpLog_idx` ON `llevame`.`servicio` (`idEmpLog` ASC);

CREATE INDEX `idEmpOp_idx` ON `llevame`.`servicio` (`idEmpOp` ASC);


-- -----------------------------------------------------
-- Table `llevame`.`voucher`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`voucher` ;

CREATE TABLE IF NOT EXISTS `llevame`.`voucher` (
  `idVoucher` INT NULL AUTO_INCREMENT,
  `origen` VARCHAR(45) NULL,
  `destino` VARCHAR(45) NULL,
  `fecha` DATE NULL,
  `hora` VARCHAR(45) NULL,
  `idLogistico` INT NOT NULL,
  `idOperador` INT NULL,
  `idPrestador` INT NULL,
  `idEstado` INT NULL,
  `idServicio` INT NOT NULL,
  `idEmpOp` INT NULL,
  `idEmpLog` INT NOT NULL,
  PRIMARY KEY (`idVoucher`),
  
    FOREIGN KEY (`idLogistico`)
    REFERENCES `llevame`.`logistico` (`idLogistico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idPrestador`)
    REFERENCES `llevame`.`prestador` (`idPrestador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEstado`)
    REFERENCES `llevame`.`estado` (`idEstado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idOperador`)
    REFERENCES `llevame`.`operador` (`idOperador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idServicio`)
    REFERENCES `llevame`.`servicio` (`idServicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpOp`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpLog`)
    REFERENCES `llevame`.`empresaLogistica` (`idEmpLogistica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idLogistico_idx` ON `llevame`.`voucher` (`idLogistico` ASC);

CREATE INDEX `idPrestador_idx` ON `llevame`.`voucher` (`idPrestador` ASC);

CREATE INDEX `idEstado_idx` ON `llevame`.`voucher` (`idEstado` ASC);

CREATE INDEX `idOperador_idx` ON `llevame`.`voucher` (`idOperador` ASC);

CREATE INDEX `idServicio_idx` ON `llevame`.`voucher` (`idServicio` ASC);

CREATE INDEX `idEmpOp_idx` ON `llevame`.`voucher` (`idEmpOp` ASC);

CREATE INDEX `idEmpLog_idx` ON `llevame`.`voucher` (`idEmpLog` ASC);


-- -----------------------------------------------------
-- Table `llevame`.`brinda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`brinda` ;

CREATE TABLE IF NOT EXISTS `llevame`.`brinda` (
  `idPrestador` INT NOT NULL,
  `idTipoServicio` INT NOT NULL,
  `idEmpOp` INT NOT NULL,
  PRIMARY KEY (`idPrestador`, `idTipoServicio`, `idEmpOp`),
  
    FOREIGN KEY (`idPrestador`)
    REFERENCES `llevame`.`prestador` (`idPrestador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idTipoServicio`)
    REFERENCES `llevame`.`tipoServicio` (`idTipoServicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpOp`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idTipoServicio_idx` ON `llevame`.`brinda` (`idTipoServicio` ASC);

CREATE INDEX `idEmpOp_idx` ON `llevame`.`brinda` (`idEmpOp` ASC);


-- -----------------------------------------------------
-- Table `llevame`.`trabaja`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `llevame`.`trabaja` ;

CREATE TABLE IF NOT EXISTS `llevame`.`trabaja` (
  `idEmpOperadora` INT NOT NULL,
  `idEmpLogistica` INT NOT NULL,
  PRIMARY KEY (`idEmpOperadora`, `idEmpLogistica`),
  
    FOREIGN KEY (`idEmpOperadora`)
    REFERENCES `llevame`.`empresaOperadora` (`idEmpOperadora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (`idEmpLogistica`)
    REFERENCES `llevame`.`empresaLogistica` (`idEmpLogistica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idEmpLogistica_idx` ON `llevame`.`trabaja` (`idEmpLogistica` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
