<?php

namespace app\controllers;

use Yii;
use app\models\Brinda;
use app\models\BrindaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BrindaController implements the CRUD actions for Brinda model.
 */
class BrindaController extends Controller
{
    public function behaviors()
    {
        return [
            
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brinda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrindaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Brinda model.
     * @param integer $idPrestador
     * @param integer $idTipoServicio
     * @return mixed
     */
    public function actionView($idPrestador, $idTipoServicio)
    {
        return $this->render('view', [
            'model' => $this->findModel($idPrestador, $idTipoServicio),
        ]);
    }

    /**
     * Creates a new Brinda model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brinda;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idPrestador' => $model->idPrestador, 'idTipoServicio' => $model->idTipoServicio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Brinda model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idPrestador
     * @param integer $idTipoServicio
     * @return mixed
     */
    public function actionUpdate($idPrestador, $idTipoServicio)
    {
        $model = $this->findModel($idPrestador, $idTipoServicio);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idPrestador' => $model->idPrestador, 'idTipoServicio' => $model->idTipoServicio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Brinda model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idPrestador
     * @param integer $idTipoServicio
     * @return mixed
     */
    public function actionDelete($idPrestador, $idTipoServicio)
    {
        $this->findModel($idPrestador, $idTipoServicio)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brinda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idPrestador
     * @param integer $idTipoServicio
     * @return Brinda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idPrestador, $idTipoServicio)
    {
        if (($model = Brinda::findOne(['idPrestador' => $idPrestador, 'idTipoServicio' => $idTipoServicio])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
