<?php

namespace app\controllers;

use Yii;
use app\models\UsuarioEmpresa;
use app\models\UsuarioEmpresaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UsuarioEmpresaController implements the CRUD actions for UsuarioEmpresa model.
 */
class UsuarioEmpresaController extends Controller
{
    public function behaviors()
    {
        return [
            
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsuarioEmpresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioEmpresaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UsuarioEmpresa model.
     * @param integer $idUsuario
     * @param integer $idEmpLog
     * @param integer $idEmpOp
     * @return mixed
     */
    public function actionView($idUsuario, $idEmpLog, $idEmpOp)
    {
        return $this->render('view', [
            'model' => $this->findModel($idUsuario, $idEmpLog, $idEmpOp),
        ]);
    }

    /**
     * Creates a new UsuarioEmpresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsuarioEmpresa;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUsuario' => $model->idUsuario, 'idEmpLog' => $model->idEmpLog, 'idEmpOp' => $model->idEmpOp]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UsuarioEmpresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idUsuario
     * @param integer $idEmpLog
     * @param integer $idEmpOp
     * @return mixed
     */
    public function actionUpdate($idUsuario, $idEmpLog, $idEmpOp)
    {
        $model = $this->findModel($idUsuario, $idEmpLog, $idEmpOp);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUsuario' => $model->idUsuario, 'idEmpLog' => $model->idEmpLog, 'idEmpOp' => $model->idEmpOp]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsuarioEmpresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idUsuario
     * @param integer $idEmpLog
     * @param integer $idEmpOp
     * @return mixed
     */
    public function actionDelete($idUsuario, $idEmpLog, $idEmpOp)
    {
        $this->findModel($idUsuario, $idEmpLog, $idEmpOp)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsuarioEmpresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idUsuario
     * @param integer $idEmpLog
     * @param integer $idEmpOp
     * @return UsuarioEmpresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idUsuario, $idEmpLog, $idEmpOp)
    {
        if (($model = UsuarioEmpresa::findOne(['idUsuario' => $idUsuario, 'idEmpLog' => $idEmpLog, 'idEmpOp' => $idEmpOp])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
