<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Logistico $model
 */

$this->title = 'Actualizar Logistico: ' . $model->idLogistico;
$this->params['breadcrumbs'][] = ['label' => 'Logisticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idLogistico, 'url' => ['view', 'id' => $model->idLogistico]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="logistico-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
