<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\VoucherSearch $searchModel
 */

$this->title = 'Vouchers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'idVoucher',
            'idEmpLog0.nombre',
            'idServicio0.idPaciente0.nombre',
            'idEstado0.estado',
            'origen',
            'destino',
            'fecha',
            'hora',
            // 'idLogistico',
            // 'idOperador',
            // 'idPrestador',
            // 'idEstado',
            // 'idServicio',
            // 'idEmpOp',
            // 'idEmpLog',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                'buttons' => [
                    'view' => function ($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['view', 'id'=>$model->idVoucher, 'idLogistico' => Yii::$app->session['idLogistico'], 'idEmpLog' => $model->idEmpLog]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
