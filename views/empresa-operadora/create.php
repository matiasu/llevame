<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EmpresaOperadora $model
 */

$this->title = 'Crear Empresa Operadora';
$this->params['breadcrumbs'][] = ['label' => 'Empresas Operadoras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-operadora-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
