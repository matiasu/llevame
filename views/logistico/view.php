<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var app\models\Logistico $model
 */

$this->title = $model->idLogistico;
$this->params['breadcrumbs'][] = ['label' => 'Logisticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistico-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idLogistico], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idLogistico], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLogistico',
            'idEmpLog0.nombre',
            'idUsuario0.nombre',
            'idUsuario0.apellido',
        ],
    ]) ?>
    
    
</div>
