<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class UsuEmpForm extends Model
{
    public $idUsuario;
    public $idEmpLog;
    public $idEmpOp;
    


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['idUsuario', 'idEmpLog', 'idEmpOp'], 'required'],
          
        ];
    }

    }
