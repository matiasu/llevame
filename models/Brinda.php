<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brinda".
 *
 * @property integer $idPrestador
 * @property integer $idTipoServicio
 * @property integer $idEmpOp
 *
 * @property Prestador $idPrestador0
 * @property Tiposervicio $idTipoServicio0
 * @property Empresaoperadora $idEmpOp0 
 */
class Brinda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brinda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPrestador', 'idTipoServicio', 'idEmpOp'], 'required'],
            [['idPrestador', 'idTipoServicio', 'idEmpOp'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPrestador' => 'Id Prestador',
            'idTipoServicio' => 'Id Tipo Servicio',
            'idEmpOp' => 'Id Emp Op', 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrestador0()
    {
        return $this->hasOne(Prestador::className(), ['idPrestador' => 'idPrestador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoServicio0()
    {
        return $this->hasOne(Tiposervicio::className(), ['idTipoServicio' => 'idTipoServicio']);
    }
    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getIdEmpOp0() 
   { 
       return $this->hasOne(Empresaoperadora::className(), ['idEmpOperadora' => 'idEmpOp']); 
   } 
}
