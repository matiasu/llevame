<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paciente".
 *
 * @property integer $idPaciente
 * @property string $nombre
 * @property string $apellido
 * @property integer $dni
 * @property string $mail 
 *
 * @property Servicio[] $servicios
 */
class Paciente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paciente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'dni', 'mail'], 'required'],
            [['idPaciente','dni'], 'integer'],
            [['nombre', 'apellido', 'mail'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPaciente' => 'Id Paciente',
            'nombre' => 'Paciente',
            'apellido' => 'Apellido Paciente',
            'dni' => 'Dni',
            'mail' => 'Mail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['idPaciente' => 'idPaciente']);
    }
}
