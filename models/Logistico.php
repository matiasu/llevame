<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistico".
 *
 * @property integer $idLogistico
 * @property integer $idEmpLog
 * @property integer $idUsuario
 *
 * @property Usuario $idUsuario0
 * @property EmpresaLogistica $idEmpLog0
 * @property Voucher[] $vouchers
 */
class Logistico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idEmpLog', 'idUsuario'], 'required'],
            [['idEmpLog', 'idUsuario'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLogistico' => 'Id Logistico',
            'idEmpLog' => 'Id Emp Log',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpLog0()
    {
        return $this->hasOne(EmpresaLogistica::className(), ['idEmpLogistica' => 'idEmpLog']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['idLogistico' => 'idLogistico']);
    }
}
