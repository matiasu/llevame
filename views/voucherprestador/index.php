<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\VoucherSearch $searchModel
 */

$this->title = 'Vouchers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="jumbotron">
    <p>
        <?= Html::a('Código QR', ['update'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= ''//Html::a('Cargar Voucher', ['update', 'idLogistico'=>$idLogistico, 'idEmpLog'=>$idEmpLog], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Modal::begin([
        'header' => '<h2>Carga de voucher con id del Paciente</h2>',
        'toggleButton' => ['label' => 'Cargar voucher',
                           'class' => 'btn btn-success',
            ],
        ]);
            $form = ActiveForm::begin([
                'method' => 'get',
                'action' => ['/voucherpaciente', 'idEmpOp'=>$idEmpOp],
            ]);
            //echo $form->field($servicio, 'idEmpOp')->textInput();
            //echo $form->field($servicio, 'idPaciente')->textInput();
            echo Html::input('text', 'idPaciente');
    ?>
            <div class="form-group">
            <?php echo Html::submitButton('Buscar', ['class' => 'btn btn-primary']);?>
            </div>

            <?php ActiveForm::end();

        Modal::end();
    ?>
    
</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idVoucher',
            'idEmpLog0.nombre',
            'idServicio0.idPaciente0.nombre',
            'idEstado0.estado',
            'origen',
            'destino',
            'fecha',
            'hora',
//            'idLogistico',
//            'idOperador',
//            'idPrestador',
//            'idEstado',
//            'idServicio',
//            'idEmpOp',
//            'idEmpLog',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['view', 'id'=>$model->idVoucher]);
                    }
                ],
            ],
        ],
    ]); 
            "" ?>

</div>
