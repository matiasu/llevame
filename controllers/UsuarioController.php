<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use app\models\PrestadorSearch;
use app\models\Prestador;
use app\models\OperadorSearch;
use app\models\Operador;
use app\models\LogisticoSearch;
use app\models\Logistico;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    public function behaviors()
    {
        return [
            
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    
    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        $model = $this->findModel($id);
        
        $searchUsuarioPrestadorModel = new PrestadorSearch;
        $searchUsuarioOperadorModel = new OperadorSearch;
        $searchUsuarioLogisticoModel = new LogisticoSearch;
        //$dataUsuarioProvider = $searchUsuarioModel->search(['UsuarioProyectoFuncionSearch'=>['idProyecto'=>$id]]);//Yii::$app->request->getQueryParams());

        $dataUsuarioPrestadorProvider = $searchUsuarioPrestadorModel->search(['PrestadorSearch'=>['idUsuario'=>$id]]);//Yii::$app->request->getQueryParams());
        $dataUsuarioOperadorProvider = $searchUsuarioOperadorModel->search(['OperadorSearch'=>['idUsuario'=>$id]]);
        $dataUsuarioLogisticoProvider = $searchUsuarioLogisticoModel->search(['LogisticoSearch'=>['idUsuario'=>$id]]);
        
        $modelUsuarioPrestador = $this->crearUsuarioPrestador($model);
        $modelUsuarioOperador = $this->crearUsuarioOperador($model);
        $modelUsuarioLogistico = $this->crearUsuarioLogistico($model);
        return $this->render('view', [
                    'model' => $model,
                    'modelUsuarioLogistico' => $modelUsuarioLogistico,
                    'modelUsuarioOperador' => $modelUsuarioOperador,
                    'modelUsuarioPrestador' => $modelUsuarioPrestador,
                    
                    'dataUsuarioOperadorProvider' => $dataUsuarioOperadorProvider,
                    'dataUsuarioLogisticoProvider' => $dataUsuarioLogisticoProvider, 
                    'dataUsuarioPrestadorProvider' => $dataUsuarioPrestadorProvider,
                    
                    'searchUsuarioPrestadorModel' => $searchUsuarioPrestadorModel,
                    'searchUsuarioOperadorModel' => $searchUsuarioOperadorModel,
                    'searchUsuarioLogisticoModel' => $searchUsuarioLogisticoModel,
        ]);
    }
    
    protected function crearUsuarioPrestador($usu) {
        $model = new Prestador;
        $model->idUsuario = $usu->idUsuario;
        
        if ($model->load(Yii::$app->request->post())){
//            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
//                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
//            }
            if ($model->save()) {
            //flash
                
            }
        }
        
        return $model;
    }
    
     protected function crearUsuarioOperador($usu) {
        $model = new Operador;
        $model->idUsuario = $usu->idUsuario;
        
        if ($model->load(Yii::$app->request->post())){
//            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
//                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
//            }
            if ($model->save()) {
            //flash
                
            }
        }
        
        return $model;
    }
    
     protected function crearUsuarioLogistico($usu) {
        $model = new Logistico;
        $model->idUsuario = $usu->idUsuario;
        
        if ($model->load(Yii::$app->request->post())){
//            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
//                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
//            }
            if ($model->save()) {
            //flash
                
            }
        }
        
        return $model;
    }
    
    

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idUsuario]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idUsuario]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
