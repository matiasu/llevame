<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Paciente;
use app\models\Trabaja;
use app\models\tipoServicio;

/**
 * @var yii\web\View $this
 * @var app\models\Servicio $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="servicio-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= "" //$form->field($model, 'idServicio')->textInput() ?>

    <?= $form->field($model, 'detalles')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'idPaciente')->dropDownList(ArrayHelper::map(Paciente::find()->all(),'idPaciente','nombre')) ?>

    <?= $form->field($model, 'idTipoServicio')->dropDownList(ArrayHelper::map(tipoServicio::find()->all(),'idTipoServicio','nombre')) ?>
    
    <?= $form->field($model, 'idEmpOp')->dropDownList(ArrayHelper::map(Trabaja::find()->all(),'idEmpOperadora','idEmpOperadora0.nombre')) ?>

    <?= $form->field($model, 'creditos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
