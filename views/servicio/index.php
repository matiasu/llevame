<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\ServicioSearch $searchModel
 */

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Servicio', ['create', 'idLogistico' => $idLogistico, 'idEmpLog' => $idEmpLog], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idServicio',
            'detalles',
            'creditos',
            'idPaciente0.nombre',
            'idTipoServicio0.nombre',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['view', 'id'=>$model->idServicio, 'idLogistico' => Yii::$app->session['idLogistico'], 'idEmpLog' => $model->idEmpLog]);
                    }
                ],
                
            ],
        ],
    ]); ?>

</div>
