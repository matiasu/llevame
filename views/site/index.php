<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */
$this->title = 'Llevame!';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bienvenido <?= Yii::$app->user->identity->username?>!</h1>
        <p>
        <?php if(!Yii::$app->user->isGuest){
            echo '<img src="../web/imagen/llevame.png" style="width:100%;" border="0" width="900">';
            if (!Yii::$app->user->identity->esAdm){
                echo '<p><h2>Rol/es asignado/s a su Usuario</h2></p>';
            }
        }
        ?>
        </p>
<!--        <p class="lead">You have successfully created your Yii-powered application.</p>-->
        
        <?php foreach ($logisticos as $logistico):
            /**
             *  @var Logistico $logistico
             */
            ?>
             
                <?= Html::a('Logistico '.$logistico->idEmpLog0->nombre, ['servicio/index', 'idLogistico'=>$logistico->idLogistico, 'idEmpLog'=>$logistico->idEmpLog], ['class' => 'btn btn-success']) ?>
             
        <?php endforeach;?>
        <?php foreach ($operadores as $operador):
            /**
             *  @var Operador $operador
             */
            ?>
             <p>
            <?= Html::a('Operador '.$operador->idEmpOp0->nombre, ['voucheroperador/index', 'idOperador'=>$operador->idOperador, 'idEmpOp'=>$operador->idEmpOp], ['class' => 'btn btn-warning']) ?> 
             </p> 
        <?php endforeach;?>
        <?php foreach ($prestadores as $prestador):
            /**
             *  @var Prestador $prestador
             */
            ?>
            <p>
            <?= Html::a('Prestador '.$prestador->idEmpOp0->nombre, ['voucherprestador/index', 'idPrestador'=>$prestador->idPrestador, 'idEmpOp'=>$prestador->idEmpOp], ['class' => 'btn btn-danger']) ?> 
          </p> 
        <?php endforeach;?>
        
        
    </div>

    <div class="body-content">
       
    </div>
</div>
