<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio".
 *
 * @property integer $idServicio
 * @property string $detalles
 * @property integer $creditos
 * @property integer $idPaciente
 * @property integer $idTipoServicio
 * @property integer $idEmpLog
 * @property integer $idEmpOp
 *
 * @property Paciente $idPaciente0
 * @property Tiposervicio $idTipoServicio0
 * @property Empresalogistica $idEmpLog0
 * @property Empresaoperadora $idEmpOp0
 * @property Voucher[] $vouchers
 */
class Servicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['detalles', 'idPaciente', 'idTipoServicio', 'idEmpLog', 'idEmpOp'], 'required'],
            [['creditos', 'idPaciente', 'idTipoServicio', 'idEmpLog', 'idEmpOp'], 'integer'],
            [['detalles'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idServicio' => 'Id Servicio',
            'detalles' => 'Detalles',
            'creditos' => 'Creditos',
            'idPaciente' => 'Id Paciente',
            'idTipoServicio' => 'Id Tipo Servicio',
            'idEmpLog' => 'Id Emp Log',
            'idEmpOp' => 'Id Emp Op',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente0()
    {
        return $this->hasOne(Paciente::className(), ['idPaciente' => 'idPaciente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoServicio0()
    {
        return $this->hasOne(Tiposervicio::className(), ['idTipoServicio' => 'idTipoServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpLog0()
    {
        return $this->hasOne(Empresalogistica::className(), ['idEmpLogistica' => 'idEmpLog']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpOp0()
    {
        return $this->hasOne(Empresaoperadora::className(), ['idEmpOperadora' => 'idEmpOp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['idServicio' => 'idServicio']);
    }
}
