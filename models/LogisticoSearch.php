<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logistico;

/**
 * LogisticoSearch represents the model behind the search form about `app\models\Logistico`.
 */
class LogisticoSearch extends Logistico
{
    public function rules()
    {
        return [
            [['idLogistico', 'idEmpLog', 'idUsuario'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Logistico::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idLogistico' => $this->idLogistico,
            'idEmpLog' => $this->idEmpLog,
            'idUsuario' => $this->idUsuario,
        ]);

        return $dataProvider;
    }
}
