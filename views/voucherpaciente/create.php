<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Voucher $model
 */

$this->title = 'Crear Voucher';
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index', 'idLogistico' => $model->idLogistico, 'idEmpLog' => $model->idEmpLog]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formu', [
        'model' => $model,
        'empOp' => $empOp,
        //'modelUsuario' => $modelUsuario
    ]) ?>

</div>
