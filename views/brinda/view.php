<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Brinda $model
 */

$this->title = $model->idPrestador0->idUsuario0->nombre." - ".$model->idTipoServicio0->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Brinda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brinda-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idPrestador' => $model->idPrestador, 'idTipoServicio' => $model->idTipoServicio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idPrestador' => $model->idPrestador, 'idTipoServicio' => $model->idTipoServicio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPrestador0.idUsuario0.nombre',
            'idTipoServicio0.nombre',
        ],
    ]) ?>

</div>
