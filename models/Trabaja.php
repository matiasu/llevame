<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabaja".
 *
 * @property integer $idEmpOperadora
 * @property integer $idEmpLogistica
 *
 * @property EmpresaOperadora $idEmpOperadora0
 * @property EmpresaLogistica $idEmpLogistica0
 */
class Trabaja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trabaja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idEmpOperadora', 'idEmpLogistica'], 'required'],
            [['idEmpOperadora', 'idEmpLogistica'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEmpOperadora' => 'Id Emp Operadora',
            'idEmpLogistica' => 'Id Emp Logistica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpOperadora0()
    {
        return $this->hasOne(EmpresaOperadora::className(), ['idEmpOperadora' => 'idEmpOperadora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpLogistica0()
    {
        return $this->hasOne(EmpresaLogistica::className(), ['idEmpLogistica' => 'idEmpLogistica']);
    }
}
