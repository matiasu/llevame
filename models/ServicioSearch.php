<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servicio;

/**
 * ServicioSearch represents the model behind the search form about `app\models\Servicio`.
 */
class ServicioSearch extends Servicio
{
    public function rules()
    {
        return [
            [['idServicio', 'creditos', 'idPaciente', 'idTipoServicio', 'idEmpLog'], 'integer'],
            [['detalles'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Servicio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idServicio' => $this->idServicio,
            'creditos' => $this->creditos,
            'idPaciente' => $this->idPaciente,
            'idTipoServicio' => $this->idTipoServicio,
            'idEmpLog' => $this->idEmpLog,
        ]);

        $query->andFilterWhere(['like', 'detalles', $this->detalles]);

        return $dataProvider;
    }
}
