<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Prestador $model
 */

$this->title = 'Crear Prestador';
$this->params['breadcrumbs'][] = ['label' => 'Prestadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
