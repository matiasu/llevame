<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogisticoSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="logistico-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idLogistico') ?>

    <?= $form->field($model, 'idEmpresaLog') ?>

    <?= $form->field($model, 'idUsuario') ?>

    <?= $form->field($model, 'idRol') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
