<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\PrestadorSearch $searchModel
 */

$this->title = 'Prestador';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestador-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Prestador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idPrestador',
            'idEmpOp0.nombre',
            'idUsuario0.nombre',
            'idUsuario0.apellido',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
