<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\Usuario $model
 */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idUsuario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idUsuario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idUsuario',
            'nombre',
            'apellido',
            'clave',
        ],
    ]) ?>
    
    <br>
    <?php echo Tabs::widget([
    'items' => [
        [
            'label' => 'Prestador',
            'content' => $this->render('_prestadorview',[
                    'modelUsuarioPrestador' => $modelUsuarioPrestador,
                    'dataUsuarioPrestadorProvider' => $dataUsuarioPrestadorProvider,
                    'searchUsuarioPrestadorModel' => $searchUsuarioPrestadorModel,
                    ]),
            'active' => true
        ],
        [
            'label' => 'Operador',
            'content' => $this->render('_operadorview',[
                'modelUsuarioOperador' => $modelUsuarioOperador,
                'dataUsuarioOperadorProvider' => $dataUsuarioOperadorProvider,
                'searchUsuarioOperadorModel' => $searchUsuarioOperadorModel,
            ]),
            
        ],
        [
            'label' => 'Logistico',
            'content' => $this->render('_logisticoview',[
                'modelUsuarioLogistico' => $modelUsuarioLogistico,
                'dataUsuarioLogisticoProvider' => $dataUsuarioLogisticoProvider,
                'searchUsuarioLogisticoModel' => $searchUsuarioLogisticoModel,
            ]),
        ],
    ],
]);?>
    
    
        
    
</div>
