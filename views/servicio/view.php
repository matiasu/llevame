<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Servicio $model
 */

$this->title = $model->detalles." - ".$model->idPaciente0->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index','idLogistico'=>Yii::$app->session['idLogistico'],'idEmpLog'=>Yii::$app->session['idEmpLog']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idServicio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->idServicio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idServicio',
            'detalles',
            'creditos',
            'idPaciente0.nombre',
            'idTipoServicio0.nombre',
        ],
    ]) ?>
    
    <p>
        <?= Html::a('Ir a vouchers', ['voucherlogistico/index', 'idServicio' => $model->idServicio], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
