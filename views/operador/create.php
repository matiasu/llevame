<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Operador $model
 */

$this->title = 'Crear Operador';
$this->params['breadcrumbs'][] = ['label' => 'Operadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
