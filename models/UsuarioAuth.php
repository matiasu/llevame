<?php
namespace app\models;
use Yii;


class UsuarioAuth  {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($idUsuario,$operation, $params = array()) {
        
        
        if (\Yii::$app->user->isGuest ) {
            // Not identified => no rights
            return false;
        }
        if (\Yii::$app->user->identity->esAdm) {
            return true; // admin role has access to everything
        }
        
        if (isset(Yii::$app->session['rol']))
        {
            $role = Yii::$app->session['rol'];
        }else{
            $role = "";
        }
        
        /**
         * chequeo si es usuario asignado al proyecto
         */
        //print_r($id);print_r($operation);        print_r($params);        exit();
//        if ($operation=== 'usuarioVinculadoProyecto') {
//            /* @var Usuario $usuario */
//            if (isset($params['Proyecto'])) {
//                $proyecto = $params['Proyecto'];
//                if (isset($params['Funcion'])) {
//                    $idFuncion = $params['Funcion'];
//                } else {
//                    $idFuncion = null;
//                }
//                //$idUsuario = Yii::$app->user->idUsuario;
//                return $proyecto->usuarioVinculado($idUsuario, $idFuncion);
//            }
//        }
                
        // allow access if the operation request is the current user's role
        return ($operation === $role);
    }

}
