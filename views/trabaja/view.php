<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Trabaja $model
 */

$this->title = $model->idEmpOperadora;
$this->params['breadcrumbs'][] = ['label' => 'Empresas Asociadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabaja-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idEmpOperadora' => $model->idEmpOperadora, 'idEmpLogistica' => $model->idEmpLogistica], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idEmpOperadora' => $model->idEmpOperadora, 'idEmpLogistica' => $model->idEmpLogistica], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Esta seguro que quiere borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idEmpOperadora0.nombre',
            'idEmpLogistica0.nombre',
        ],
    ]) ?>

</div>
