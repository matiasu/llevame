<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposervicio".
 *
 * @property integer $idTipoServicio
 * @property string $nombre
 *
 * @property Brinda $brinda
 * @property Prestador[] $idPrestadors
 * @property Servicio[] $servicios
 */
class Tiposervicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiposervicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipoServicio' => 'Id Tipo Servicio',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrinda()
    {
        return $this->hasOne(Brinda::className(), ['idTipoServicio' => 'idTipoServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrestadors()
    {
        return $this->hasMany(Prestador::className(), ['idPrestador' => 'idPrestador'])->viaTable('brinda', ['idTipoServicio' => 'idTipoServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['idTipoServicio' => 'idTipoServicio']);
    }
}
