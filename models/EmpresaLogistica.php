<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresaLogistica".
 *
 * @property integer $idEmpLogistica
 * @property string $nombre
 *
 * @property Logistico[] $logisticos
 * @property Servicio[] $servicios 
 * @property Trabaja $trabaja 
 * @property EmpresaOperadora[] $idEmpOperadoras 
 * @property Voucher[] $vouchers
 */
class Empresalogistica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empresaLogistica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEmpLogistica' => 'Id Emp Logistica',
            'nombre' => 'Empresa Logistica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticos()
    {
        return $this->hasMany(Logistico::className(), ['idEmpLog' => 'idEmpLogistica']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
   public function getServicios()
   {
       return $this->hasMany(Servicio::className(), ['idEmpLog' => 'idEmpLogistica']);
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getTrabaja()
   {
       return $this->hasOne(Trabaja::className(), ['idEmpLogistica' => 'idEmpLogistica']);
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getIdEmpOperadoras()
   {
       return $this->hasMany(EmpresaOperadora::className(), ['idEmpOperadora' => 'idEmpOperadora'])->viaTable('trabaja', ['idEmpLogistica' => 'idEmpLogistica']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['idEmpLog' => 'idEmpLogistica']);
    }
}
