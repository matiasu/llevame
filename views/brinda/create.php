<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Brinda $model
 */

$this->title = 'Crear Brinda';
$this->params['breadcrumbs'][] = ['label' => 'Brinda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brinda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
