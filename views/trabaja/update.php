<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Trabaja $model
 */

$this->title = 'Actualizar Trabaja: ' . $model->idEmpOperadora;
$this->params['breadcrumbs'][] = ['label' => 'Trabajas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEmpOperadora, 'url' => ['view', 'idEmpOperadora' => $model->idEmpOperadora, 'idEmpLogistica' => $model->idEmpLogistica]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trabaja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
