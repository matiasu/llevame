<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voucher".
 *
 * @property integer $idVoucher
 * @property string $origen
 * @property string $destino
 * @property string $fecha
 * @property string $hora
 * @property integer $idLogistico
 * @property integer $idOperador
 * @property integer $idPrestador
 * @property integer $idEstado
 * @property integer $idServicio
 * @property integer $idEmpOp
 * @property integer $idEmpLog
 *
 * @property Logistico $idLogistico0
 * @property Prestador $idPrestador0
 * @property Estado $idEstado0
 * @property Operador $idOperador0
 * @property Servicio $idServicio0
 * @property Empresaoperadora $idEmpOp0
 * @property Empresalogistica $idEmpLog0
 */
class Voucher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voucher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idLogistico', 'idServicio', 'idEmpLog'], 'required'],
            [['idVoucher', 'idLogistico', 'idOperador', 'idPrestador', 'idEstado', 'idServicio', 'idEmpOp', 'idEmpLog'], 'integer'],
            [['fecha'], 'safe'],
            [['origen', 'destino'], 'string', 'max' => 45],
            [['hora'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idVoucher' => 'Id Voucher',
            'origen' => 'Origen',
            'destino' => 'Destino',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'idLogistico' => 'Id Logistico',
            'idOperador' => 'Id Operador',
            'idPrestador' => 'Id Prestador',
            'idEstado' => 'Id Estado',
            'idServicio' => 'Id Servicio',
            'idEmpOp' => 'Id Emp Op',
            'idEmpLog' => 'Id Emp Log',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLogistico0()
    {
        return $this->hasOne(Logistico::className(), ['idLogistico' => 'idLogistico']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrestador0()
    {
        return $this->hasOne(Prestador::className(), ['idPrestador' => 'idPrestador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado0()
    {
        return $this->hasOne(Estado::className(), ['idEstado' => 'idEstado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOperador0()
    {
        return $this->hasOne(Operador::className(), ['idOperador' => 'idOperador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdServicio0()
    {
        return $this->hasOne(Servicio::className(), ['idServicio' => 'idServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpOp0()
    {
        return $this->hasOne(Empresaoperadora::className(), ['idEmpOperadora' => 'idEmpOp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpLog0()
    {
        return $this->hasOne(Empresalogistica::className(), ['idEmpLogistica' => 'idEmpLog']);
    }
}
