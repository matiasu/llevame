<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ServicioSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="servicio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idServicio') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'creditos') ?>

    <?= $form->field($model, 'idPaciente') ?>

    <?= $form->field($model, 'idTipoServicio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
