<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Trabaja $model
 */

$this->title = 'Crear Relacion de Trabajo';
$this->params['breadcrumbs'][] = ['label' => 'Empresas Asociadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabaja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
