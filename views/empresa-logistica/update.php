<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EmpresaLogistica $model
 */

$this->title = 'Actualizar Empresa Logistica: ' . $model->idEmpLogistica;
$this->params['breadcrumbs'][] = ['label' => 'Empresas Logisticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEmpLogistica, 'url' => ['view', 'id' => $model->idEmpLogistica]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="empresa-logistica-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
