<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\EmpresaOperadora;

/**
 * @var yii\web\View $this
 * @var app\models\Operador $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="operador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEmpOp')->dropDownList(ArrayHelper::map(EmpresaOperadora::find()->all(),'idEmpOperadora','nombre')) ?>

    <?= ""//$form->field($model, 'idUsuario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
