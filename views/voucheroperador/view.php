<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Voucher $model
 */

$this->title = $model->idVoucher;
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index', 'idOperador'=>Yii::$app->session['idOperador'], 'idEmpOp'=>Yii::$app->session['idEmpOp']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idVoucher], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idVoucher',
            'origen',
            'destino',
            'fecha',
            'hora',
            //'idLogistico',
            //'idOperador',
            'idPrestador0.idUsuario0.nombre',
            'idEstado0.estado',
            'idServicio.detalles',
            'idEmpOp0.nombre',
            'idEmpLog0.nombre',
        ],
    ]) ?>

</div>
