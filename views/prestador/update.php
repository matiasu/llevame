<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Prestador $model
 */

$this->title = 'Actualizar Prestador: ' . $model->idPrestador;
$this->params['breadcrumbs'][] = ['label' => 'Prestadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPrestador, 'url' => ['view', 'id' => $model->idPrestador]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="prestador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
