<?php

namespace app\controllers;

use Yii;
use app\models\Servicio;
use app\models\ServicioSearch;
use app\models\Voucher;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class ServicioController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','delete', 'create','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['logistico'],
                    ],
                    [
                        'actions' => ['index','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Servicio models.
     * @return mixed
     */
    public function actionIndex($idLogistico, $idEmpLog)
    {   
        Yii::$app->session['rol']="logistico";
        Yii::$app->session['idLogistico']=$idLogistico;
        Yii::$app->session['idEmpLog']=$idEmpLog;
        
        $searchModel = new ServicioSearch;
        $dataProvider = $searchModel->search(['ServicioSearch'=>['idEmpLog' => $idEmpLog]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'idLogistico' => $idLogistico,
            'idEmpLog' => $idEmpLog,
        ]);
    }

    /**
     * Displays a single Servicio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
//            'idLogistico' => $idLogistico,
//            'idEmpLog' => $idEmpLog,
            
        ]);
    }

    /**
     * Creates a new Servicio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Servicio;
        $model->idEmpLog = Yii::$app->session['idEmpLog'];
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            
            
                for ($i = 0; $i < $model->creditos; $i++){
                    $voucher = new Voucher;
                    $voucher->idServicio = $model->idServicio;
                    $voucher->idLogistico = Yii::$app->session['idLogistico'];
                    $voucher->idEmpLog = Yii::$app->session['idEmpLog'];
                    $voucher->idEstado = 3;
                    $voucher->idEmpOp = $model->idEmpOp;
                    $voucher->save();
                }
            
            
            return $this->redirect(['view', 'id' => $model->idServicio]);
        } else {
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Updates an existing Servicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idServicio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Servicio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'idLogistico' => Yii::$app->session['idLogistico'], 'idEmpLog' => Yii::$app->session['idEmpLog']]);
    }

    /**
     * Finds the Servicio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Servicio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Servicio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
