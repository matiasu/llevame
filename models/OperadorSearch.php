<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operador;

/**
 * OperadorSearch represents the model behind the search form about `app\models\Operador`.
 */
class OperadorSearch extends Operador
{
    public function rules()
    {
        return [
            [['idOperador', 'idEmpOp', 'idUsuario'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Operador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idOperador' => $this->idOperador,
            'idEmpOp' => $this->idEmpOp,
            'idUsuario' => $this->idUsuario,
        ]);

        return $dataProvider;
    }
}
