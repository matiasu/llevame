<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpresaOperadora;

/**
 * EmpresaOperadoraSearch represents the model behind the search form about `app\models\EmpresaOperadora`.
 */
class EmpresaOperadoraSearch extends EmpresaOperadora
{
    public function rules()
    {
        return [
            [['idEmpOperadora'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EmpresaOperadora::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEmpOperadora' => $this->idEmpOperadora,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
