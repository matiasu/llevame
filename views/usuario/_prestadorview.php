<?php
use yii\grid\GridView;
use yii\bootstrap\Modal;
?>
<br>
<?php Modal::begin([
    'header' => '<h2>Asignar Rol</h2>',
    'toggleButton' => ['label' => 'Nuevo Rol Prestador',
                       'class' => 'btn btn-success',
        ],
    ]);
        echo $this->render('_formusuario', [
        //'modelUsuEmp' => $modelUsuEmp,
        'model' => $modelUsuarioPrestador,
    ]);

    Modal::end();?>
    
        <h2>Rol asignado al usuario</h2>
        
     <?= GridView::widget([
        'dataProvider' => $dataUsuarioPrestadorProvider,
        'filterModel' => $searchUsuarioPrestadorModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idUsuario0.nombre',
            'idEmpOp0.nombre',
           
            //'idUsuario0.idEmpOp',
            //'idFuncion0.Funcion',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',
                'controller'=>'prestador',
                ]
        ],
    ]); ?>