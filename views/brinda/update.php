<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Brinda $model
 */

$this->title = 'Update Brinda: ' . $model->idPrestador;
$this->params['breadcrumbs'][] = ['label' => 'Brinda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPrestador, 'url' => ['view', 'idPrestador' => $model->idPrestador, 'idTipoServicio' => $model->idTipoServicio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="brinda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
