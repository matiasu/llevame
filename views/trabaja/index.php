<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\TrabajaSearch $searchModel
 */

$this->title = 'Empresas Asociadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabaja-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Trabaja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idEmpOperadora0.nombre',
            'idEmpLogistica0.nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
