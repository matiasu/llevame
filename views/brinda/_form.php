<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Prestador;
use app\models\tipoServicio;

/**
 * @var yii\web\View $this
 * @var app\models\Brinda $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="brinda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPrestador')->dropDownList(ArrayHelper::map(Prestador::find()->all(),'idPrestador','idUsuario0.nombre')) ?>

    <?= $form->field($model, 'idTipoServicio')->dropDownList(ArrayHelper::map(tipoServicio::find()->all(),'idTipoServicio','nombre')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
