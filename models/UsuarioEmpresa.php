<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarioempresa".
 *
 * @property integer $idUsuario
 * @property integer $idEmpLog
 * @property integer $idEmpOp
 */
class Usuarioempresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarioempresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idEmpLog', 'idEmpOp'], 'required'],
            [['idUsuario', 'idEmpLog', 'idEmpOp'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idEmpLog' => 'Id Emp Log',
            'idEmpOp' => 'Id Emp Op',
        ];
    }
}
