<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Logistico $model
 */

$this->title = 'Crear Logistico';
$this->params['breadcrumbs'][] = ['label' => 'Logisticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formlogistico', [
        'model' => $model,
    ]) ?>

</div>
