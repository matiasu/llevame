<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresaoperadora".
 *
 * @property integer $idEmpOperadora
 * @property string $nombre
 *
 * @property Brinda $brinda 
 * @property Operador[] $operadors
 * @property Prestador[] $prestadors
 * @property Servicio[] $servicios 
 * @property Trabaja $trabaja 
 * @property Empresalogistica[] $idEmpLogisticas 
 * @property Voucher[] $vouchers
 */
class Empresaoperadora extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empresaoperadora';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEmpOperadora' => 'Id Emp Operadora',
            'nombre' => 'Empresa Operadora',
        ];
    }
    
    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getBrinda() 
    { 
        return $this->hasOne(Brinda::className(), ['idEmpOp' => 'idEmpOperadora']); 
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperadors()
    {
        return $this->hasMany(Operador::className(), ['idEmpOp' => 'idEmpOperadora']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestadors()
    {
        return $this->hasMany(Prestador::className(), ['idEmpOp' => 'idEmpOperadora']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getServicios() 
    { 
        return $this->hasMany(Servicio::className(), ['idEmpOp' => 'idEmpOperadora']); 
    } 
 
   /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getTrabaja() 
    { 
        return $this->hasOne(Trabaja::className(), ['idEmpOperadora' => 'idEmpOperadora']); 
    } 
 
   /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getIdEmpLogisticas() 
    { 
        return $this->hasMany(Empresalogistica::className(), ['idEmpLogistica' => 'idEmpLogistica'])->viaTable('trabaja', ['idEmpOperadora' => 'idEmpOperadora']); 
    } 
 
   /** 
    * @return \yii\db\ActiveQuery 
    */ 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['idEmpOp' => 'idEmpOperadora']);
    }
}
