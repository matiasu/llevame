<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Brinda;

/**
 * BrindaSearch represents the model behind the search form about `app\models\Brinda`.
 */
class BrindaSearch extends Brinda
{
    public function rules()
    {
        return [
            [['idPrestador', 'idTipoServicio'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Brinda::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idPrestador' => $this->idPrestador,
            'idTipoServicio' => $this->idTipoServicio,
        ]);

        return $dataProvider;
    }
}
