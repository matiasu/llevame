<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Operador $model
 */

$this->title = 'Actualizar Operador: ' . $model->idOperador;
$this->params['breadcrumbs'][] = ['label' => 'Operadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idOperador, 'url' => ['view', 'id' => $model->idOperador]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="operador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
