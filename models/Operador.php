<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operador".
 *
 * @property integer $idOperador
 * @property integer $idEmpOp
 * @property integer $idUsuario
 *
 * @property EmpresaOperadora $idEmpOp0
 * @property Usuario $idUsuario0
 * @property Voucher[] $vouchers
 */
class Operador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idEmpOp', 'idUsuario'], 'required'],
            [['idEmpOp', 'idUsuario'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idOperador' => 'Id Operador',
            'idEmpOp' => 'Id Emp Op',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpOp0()
    {
        return $this->hasOne(EmpresaOperadora::className(), ['idEmpOperadora' => 'idEmpOp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['idOperador' => 'idOperador']);
    }
}
