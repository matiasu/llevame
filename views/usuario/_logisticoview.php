<?php
use yii\grid\GridView;
use yii\bootstrap\Modal;
?>
<br>
<?php Modal::begin([
    'header' => '<h2>Asignar Rol</h2>',
    'toggleButton' => ['label' => 'Nuevo Rol Logistico',
                       'class' => 'btn btn-success',
        ],
    ]);
        echo $this->render('_formusuariolog', [
        //'modelUsuEmp' => $modelUsuEmp,
        'model' => $modelUsuarioLogistico,
    ]);

    Modal::end();?>
    
    <h2>Rol asignado al usuario</h2>
        
     <?= GridView::widget([
        'dataProvider' => $dataUsuarioLogisticoProvider,
        'filterModel' => $searchUsuarioLogisticoModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idUsuario0.nombre',
            'idEmpLog0.nombre',
           
            //'idUsuario0.idEmpOp',
            //'idFuncion0.Funcion',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',
                'controller'=>'logistico',
                ]
        ],
    ]); ?>

