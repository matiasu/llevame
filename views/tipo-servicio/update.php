<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\tipoServicio $model
 */

$this->title = 'Actualizar Tipo Servicio: ' . $model->idTipoServicio;
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTipoServicio, 'url' => ['view', 'id' => $model->idTipoServicio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tipo-servicio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
