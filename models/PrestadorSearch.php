<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prestador;

/**
 * PrestadorSearch represents the model behind the search form about `app\models\Prestador`.
 */
class PrestadorSearch extends Prestador
{
    public function rules()
    {
        return [
            [['idPrestador', 'idEmpOp', 'idUsuario'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$idUsuario=null)
    {
        $query = Prestador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        

        $query->andFilterWhere([
            'idPrestador' => $this->idPrestador,
            'idEmpOp' => $this->idEmpOp,
            'idUsuario' => $this->idUsuario,
        ]);
        
        if(!is_null($idUsuario)){
             $query->andFilterWhere(['idUsuario' => $idUsuario]);
        }
        return $dataProvider;
    }
}
