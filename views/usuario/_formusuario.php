<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\EmpresaLogistica;
use app\models\EmpresaOperadora;
use yii\helpers\ArrayHelper;


/**
 * @var yii\web\View $this
 * @var app\models\Prestador $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="prestador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= '' //$form->field($model, 'idUsuario')->textInput() ?>

    <?= $form->field($model, 'idEmpOp')->dropDownList(ArrayHelper::map(EmpresaOperadora::find()->all(),'idEmpOperadora','nombre')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
