<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Voucher;

/**
 * VoucherSearch represents the model behind the search form about `app\models\Voucher`.
 */
class VoucherSearch extends Voucher
{
    public function rules()
    {
        return [
            [['idVoucher', 'idLogistico', 'idOperador', 'idPrestador', 'idEstado', 'idServicio', 'idEmpOp', 'idEmpLog'], 'integer'],
            [['origen', 'destino', 'fecha', 'hora'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Voucher::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idVoucher' => $this->idVoucher,
            'fecha' => $this->fecha,
            'hora' => $this->hora,
            'idLogistico' => $this->idLogistico,
            'idOperador' => $this->idOperador,
            'idPrestador' => $this->idPrestador,
            'idEstado' => $this->idEstado,
            'idServicio' => $this->idServicio,
            'idEmpOp' => $this->idEmpOp,
            'idEmpLog' => $this->idEmpLog,
        ]);
        
        

        $query->andFilterWhere(['like', 'origen', $this->origen])
            ->andFilterWhere(['like', 'destino', $this->destino]);

        return $dataProvider;
    }
}
