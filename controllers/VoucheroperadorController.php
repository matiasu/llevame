<?php

namespace app\controllers;

use Yii;
use app\models\Voucher;
use app\models\VoucherSearch;
use app\models\Trabaja;
use app\models\EmpresaOperadora;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * VoucheroperadorController implements the CRUD actions for Voucher model.
 */
class VoucheroperadorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','index','view'],
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['operador'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex($idOperador,$idEmpOp)
    {/*
     * todo: chequear que el $idOperador sea el mismo del usuario logueado
     */
        Yii::$app->session['idOperador']=$idOperador;
        Yii::$app->session['idEmpOp']=$idEmpOp;
        Yii::$app->session['rol']="operador";
        $model = EmpresaOperadora::findOne($idEmpOp);
        $searchModel = new VoucherSearch;
        $dataProvider = $searchModel->search(['VoucherSearch'=>['idEmpOp'=>$idEmpOp,'idEstado'=>3]]);
        //$searchactivosModel = new VoucherSearch;
        $dataactivosProvider = $searchModel->search(['VoucherSearch'=>['idEmpOp'=>$idEmpOp,'idEstado'=>1]]);
        //$searchvoucherModel = new VoucherSearch;
        $datavoucherProvider = $searchModel->search(['VoucherSearch'=>['idEmpOp'=>$idEmpOp,'idEstado'=>2]]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'empresaLogistica' => $model,
            //'searchvoucherModel' => $searchvoucherModel,
            'datavoucherProvider' => $datavoucherProvider,
            'dataactivosProvider' => $dataactivosProvider,
            
//            'idOperador' => $idOperador,
//            'idEmpOp' => $idEmpOp,
        ]);
    }

    /**
     * Displays a single Voucher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idLogistico,$idEmpOp)
    {
        //$usuario = ;
        $emOp = Trabaja::findAll($idEmpOp);
        $model = new Voucher;
        $model->idLogistico = $idLogistico;
        $model->idEmpLog = $idEmpOp;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idVoucher]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'empOp' => $emOp,
                
            ]);
        }
    }

    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
        $model->idOperador = Yii::$app->session['idOperador'];
        if ($model->load(Yii::$app->request->post())) {
            if ($model->fecha<date('Y-m-d')){
                throw new NotFoundHttpException('No se permite crear el servicio porque la fecha es anterior a la actual');
            }else{
                $model->idEstado = 1;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->idVoucher]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Voucher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voucher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
