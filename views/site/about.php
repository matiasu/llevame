<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */
$this->title = 'Llevame!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    
    <img src="../web/imagen/llevame.png" alt="Logo Llevame!" style="width:100%;" border="0" height="300"></br><br>
    <p>
        Es una aplicación que permite consumir servicios de transporte con tan solo un <strong>código QR</strong> único para cada persona. También se puede adaptar a cualquier tipo de servicio como restaurant, hotel, espectaculos,etc.
        Implementa nuevas tecnologías para un uso práctico y sencillo, con la información siempre actualizada.<br>
        Se creó esta <strong>app</strong> para virtualizar los vouchers, con esto dejar de utilizar el papel y firmas reemplazandolos por un <strong>código QR</strong>.
    </p>

    
</div>
