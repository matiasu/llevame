<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Voucher $model
 */

$this->title = 'Actualizar Voucher: ' . $model->idVoucher;
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idVoucher, 'url' => ['view', 'id' => $model->idVoucher]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="voucher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formu', [
        'model' => $model,
    ]) ?>

</div>
