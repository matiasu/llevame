<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $idUsuario
 * @property string $nombre
 * @property string $apellido 
 * @property string $clave
 * @property integer $esAdm
 *
 * @property Logistico[] $logisticos
 * @property Operador[] $operadors
 * @property Prestador[] $prestadors
 */
class Usuario extends \yii\db\ActiveRecord
implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
   /*********** agregar para interface *********************/
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token)
    {
        return static::findOne(['access_token' => $token]);
    }
 public function getEsAdm()
    {
        return $this->esAdm;
    }

    public function getId()
    {
        return $this->idUsuario;
    }

    public function getAuthKey()
    {
        return $this->clave;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
/*agregar para funcionamiento*/    
    public static function findByUsername($username)
    {
        return static::findOne(['nombre' => $username]);
    }
    public function validatePassword($authKey)
    {
        return $this->clave === $authKey;
    }
    public function getUsername()
    {
        return $this->nombre;
    }

/********************************/    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','apellido', 'clave'], 'required'],
            [['esAdm'], 'integer'],
            [['nombre','apellido', 'clave'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'clave' => 'Clave',
            'esAdm' => 'Es Adm',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticos()
    {
        return $this->hasMany(Logistico::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperadors()
    {
        return $this->hasMany(Operador::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestadors()
    {
        return $this->hasMany(Prestador::className(), ['idUsuario' => 'idUsuario']);
    }
}
